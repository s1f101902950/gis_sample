package com.example.gis_sample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;
import android.Manifest;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.data.geojson.GeoJsonFeature;
import com.google.maps.android.data.geojson.GeoJsonParser;
import com.google.maps.android.data.geojson.GeoJsonPolygon;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.List;


public class MainActivity extends AppCompatActivity implements LocationListener {

    LocationManager locationManager;
    JSONObject json;
    public double latitude;
    public double longitude;
    int old_num=0;
    int bgm_num;
    String place_type;
    MediaPlayer mediaPlayer;
    String filePath = "sample1.mp3";
    boolean isRunning = false;
    Calendar cal = Calendar.getInstance();
    String route_log="Time: "+cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+" Area: "+place_type+"\n→";


    private final ActivityResultLauncher<String>
            requestPermissionLauncher = registerForActivityResult(
            new ActivityResultContracts.RequestPermission(),
            isGranted -> {
                if (isGranted) {
                    locationStart();
                }
                else {
                    Toast toast = Toast.makeText(this,
                            "これ以上なにもできません", Toast.LENGTH_SHORT);
                    toast.show();
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //許可の確認
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        json = getJSON();

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            requestPermissionLauncher.launch(
                    Manifest.permission.ACCESS_FINE_LOCATION);
        }
        else{
            locationStart();
        }
    }

    private JSONObject getJSON() {
        try {
            InputStream stream = this.getResources().openRawResource(R.raw.output);
            StringBuilder result = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            reader.close();
            Log.d("debug","getJSON is Completed");
            return new JSONObject(result.toString());
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            Log.d("debug","ERROR: getJSON is not Completed");
            return null;
        }
    }

    private void check(JSONObject json) {
        GeoJsonParser parser = new GeoJsonParser(json);
        for(GeoJsonFeature feature : parser.getFeatures()) {
            GeoJsonPolygon polygon = (GeoJsonPolygon) feature.getGeometry();
            List<List<LatLng>> coordinates = (List<List<LatLng>>) polygon.getGeometryObject();

            if(PolyUtil.containsLocation(latitude, longitude, coordinates.get(0), true)) {
                Iterable properties = feature.getProperties();
                //音楽指定の変数bgm_numを指定する関数
                selectNum(properties);
                Log.i("Test ", properties.toString());
            };
        }
    }

    private void selectNum(Iterable list){
        for (Object o : list){
            if(o.toString().startsWith("A29_004")){
                old_num = bgm_num;
                switch (o.toString()){
                    case "A29_004=1":
                        bgm_num=1;
                        place_type="第一種低層住居専用地域";
                        break;
                    case "A29_004=2":
                        bgm_num=2;
                        place_type="第二種低層住居専用地域";
                        break;
                    case "A29_004=3":
                        bgm_num=3;
                        place_type="第一種中高層住宅専用地域";
                        break;
                    case "A29_004=4":
                        bgm_num=4;
                        place_type="第二種中高層住居専用地域";
                        break;
                    case "A29_004=5":
                        bgm_num=5;
                        place_type="第一種住居地域";
                        break;
                    case "A29_004=6":
                        bgm_num=6;
                        place_type="第二種住宅地域";
                        break;
                    case "A29_004=7":
                        bgm_num=7;
                        place_type="準住居地域";
                        break;
                    case "A29_004=8":
                        bgm_num=8;
                        place_type="近隣商業地域";
                        break;
                    case "A29_004=9":
                        bgm_num=9;
                        place_type="商業地域";
                        break;
                    case "A29_004=10":
                        bgm_num=10;
                        place_type="準工業地域";
                        break;
                    case "A29_004=11":
                        bgm_num=11;
                        place_type="工業地域";
                        break;
                    case "A29_004=12":
                        bgm_num=12;
                        place_type="工業専用地域";
                        break;
                    default:
                        bgm_num=99;
                        place_type="不明";
                        break;
                }if(bgm_num == 8) {
                    //ルート内のエリア8が不明のバグが発生するため、今回は例外処理として取り除く
                    System.out.println("Exception Error: Area 8");
                }else if(old_num != bgm_num){
                    System.out.println("Changed bgm_num " + old_num + " to " + bgm_num);
                    TextView textView3 = findViewById(R.id.text_view3);
                    textView3.setText(place_type);
                    if(isRunning){
                        audioStop();
                        filePath= "sample"+bgm_num+".mp3";
                        //ログを記録
                        route_log+="Time: "+cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+" Area: "+place_type+"\n→";
                        TextView textView4 = findViewById(R.id.text_view4);
                        textView4.setText(route_log);
                        audioPlay(filePath);
                    }
                }else{
                    System.out.println("Not changed bgm_num " + bgm_num);
                }

            }
        }
    }

    private void locationStart(){
        Log.d("debug","locationStart()");

        // LocationManager インスタンス生成
        locationManager =
                (LocationManager) getSystemService(LOCATION_SERVICE);

        if (locationManager != null && locationManager.isProviderEnabled(
                LocationManager.GPS_PROVIDER)) {

            Log.d("debug", "location manager Enabled");
        } else {
            // GPSを設定するように促す
            Intent settingsIntent =
                    new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(settingsIntent);
            Log.d("debug", "not gpsEnable, startActivity");
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);

            Log.d("debug", "checkSelfPermission false");
            return;
        }
        //onLocationChangedは10000ミリ秒(10秒毎) && 0メートルの移動で呼ばれる？
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                5000, 30, this);

    }

    @Override
    public void onLocationChanged(Location location) {
        // 緯度の表示
        TextView textView1 = findViewById(R.id.text_view1);
        latitude = location.getLatitude();
        String str1 = "Latitude:"+latitude;
        textView1.setText(str1);

        // 経度の表示
        TextView textView2 = findViewById(R.id.text_view2);
        longitude = location.getLongitude();
        String str2 = "Longitude:"+longitude;
        textView2.setText(str2);

        //用途地域の表示
        check(json);
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private boolean audioSetup(String filePath){
        // インタンスを生成
        mediaPlayer = new MediaPlayer();
        boolean fileCheck = false;

        // assetsから mp3 ファイルを読み込み
        try(AssetFileDescriptor descriptor = getAssets().openFd(filePath))
        {
            // MediaPlayerに読み込んだ音楽ファイルを指定
            mediaPlayer.setDataSource(descriptor.getFileDescriptor(),
                    descriptor.getStartOffset(),
                    descriptor.getLength());
            // 音量調整を端末のボタンに任せる
            setVolumeControlStream(AudioManager.STREAM_MUSIC);
            mediaPlayer.prepare();
            fileCheck = true;
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        return fileCheck;
    }

    private void audioPlay(String BGMPath) {
        if (mediaPlayer == null) {
            // audio ファイルを読出し
            if (audioSetup(BGMPath)){
                Toast.makeText(getApplication(), "Rread audio file", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(getApplication(), "Error: read audio file", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        else{
            // 繰り返し再生する場合
            mediaPlayer.stop();
            mediaPlayer.reset();
            // リソースの解放
            mediaPlayer.release();
        }
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
    }

    private void audioStop() {
        // 再生終了
        mediaPlayer.stop();
        // リセット
        mediaPlayer.reset();
        // リソースの解放
        mediaPlayer.release();

        mediaPlayer = null;
    }

    public void onClick(View view){
        if(!isRunning){
            audioPlay("sample"+bgm_num+".mp3");
            TextView buttonText = findViewById(R.id.button);
            buttonText.setText("STOP");
            isRunning = true;
        }else{
            audioStop();
            TextView buttonText = findViewById(R.id.button);
            buttonText.setText("START");
            isRunning = false;
        }
    }
}